PRODUCT_BUILD_PROP_OVERRIDES += BUILD_UTC_DATE=0

ifeq ($(PRODUCT_GMS_CLIENTID_BASE),)
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.clientidbase=android-google
else
PRODUCT_PROPERTY_OVERRIDES += \
    ro.com.google.clientidbase=$(PRODUCT_GMS_CLIENTID_BASE)
endif

PRODUCT_PROPERTY_OVERRIDES += \
    keyguard.no_require_sim=true

PRODUCT_PROPERTY_OVERRIDES += \
    ro.build.selinux=1

# Disable excessive dalvik debug messages
PRODUCT_PROPERTY_OVERRIDES += \
    dalvik.vm.debug.alloc=0

# Backup tool
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/bin/backuptool.sh:install/bin/backuptool.sh \
    vendor/custom/prebuilt/common/bin/backuptool.functions:install/bin/backuptool.functions \
    vendor/custom/prebuilt/common/bin/50-custom.sh:system/addon.d/50-custom.sh \
    vendor/custom/prebuilt/common/bin/clean_cache.sh:system/bin/clean_cache.sh

ifeq ($(AB_OTA_UPDATER),true)
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/bin/backuptool_ab.sh:system/bin/backuptool_ab.sh \
    vendor/custom/prebuilt/common/bin/backuptool_ab.functions:system/bin/backuptool_ab.functions \
    vendor/custom/prebuilt/common/bin/backuptool_postinstall.sh:system/bin/backuptool_postinstall.sh
endif

# Backup services whitelist
PRODUCT_COPY_FILES += \
    vendor/custom/config/permissions/backup.xml:system/etc/sysconfig/backup.xml

# Signature compatibility validation
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/bin/otasigcheck.sh:install/bin/otasigcheck.sh

# Custom-specific init file
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/etc/init.local.rc:root/init.custom.rc

# Copy LatinIME for gesture typing
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/lib/libjni_latinimegoogle.so:system/lib/libjni_latinimegoogle.so

# SELinux filesystem labels
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/etc/init.d/50selinuxrelabel:system/etc/init.d/50selinuxrelabel

# Enable SIP+VoIP on all targets
PRODUCT_COPY_FILES += \
    frameworks/native/data/etc/android.software.sip.voip.xml:system/etc/permissions/android.software.sip.voip.xml

# Don't export PS1 in /system/etc/mkshrc.
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/etc/mkshrc:system/etc/mkshrc \
    vendor/custom/prebuilt/common/etc/sysctl.conf:system/etc/sysctl.conf

# Specific startup services
PRODUCT_COPY_FILES += \
    vendor/custom/prebuilt/common/etc/init.d/90userinit:system/etc/init.d/90userinit \
    vendor/custom/prebuilt/common/bin/sysinit:system/bin/sysinit

# Required packages
PRODUCT_PACKAGES += \
    CellBroadcastReceiver \
    Development \
    SpareParts \
    LockClock \
    su

# Optional packages
PRODUCT_PACKAGES += \
    Basic \
    LiveWallpapersPicker \
    PhaseBeam

# Include explicitly to work around GMS issues
PRODUCT_PACKAGES += \
    libprotobuf-cpp-full \
    librsjni

# AudioFX
PRODUCT_PACKAGES += \
    AudioFX

# Extra Optional packages
PRODUCT_PACKAGES += \
    Calculator \
    LatinIME \
    BluetoothExt \
    Launcher3Dark


# Extra tools
PRODUCT_PACKAGES += \
    openvpn \
    e2fsck \
    mke2fs \
    tune2fs \
    fsck.exfat \
    mkfs.exfat \
    ntfsfix \
    ntfs-3g


PRODUCT_PACKAGES += \
    charger_res_images

# Stagefright FFMPEG plugin
PRODUCT_PACKAGES += \
    libffmpeg_extractor \
    libffmpeg_omx \
    media_codecs_ffmpeg.xml

PRODUCT_PROPERTY_OVERRIDES += \
    media.sf.omx-plugin=libffmpeg_omx.so \
    media.sf.extractor-plugin=libffmpeg_extractor.so

# Storage manager
PRODUCT_PROPERTY_OVERRIDES += \
    ro.storage_manager.enabled=true

# easy way to extend to add more packages
-include vendor/extra/product.mk

PRODUCT_PACKAGES += \
    AndroidDarkThemeOverlay \
    SettingsDarkThemeOverlay

PRODUCT_PACKAGE_OVERLAYS += vendor/custom/overlay/common

# Qualcomm performance frameworks
ifeq ($(USE_QCOM_PERF),true)
    PRODUCT_BOOT_JARS += \
        QPerformance \
        UxPerformance
endif

# Themes
PRODUCT_PROPERTY_OVERRIDES += ro.boot.vendor.overlay.theme=com.google.android.theme.pixel
PRODUCT_PROPERTY_OVERRIDES += ro.com.google.ime.theme_id=5
PRODUCT_PACKAGES += \
   Pixel \
   Stock \
   Mono

# Versioning System
# custom first version.
PRODUCT_VERSION_MAINTENANCE = 1.0
CUSTOM_POSTFIX := -$(shell date +"%d_%m")
ifdef CUSTOM_BUILD_EXTRA
    CUSTOM_POSTFIX := -$(CUSTOM_BUILD_EXTRA)
endif

ifndef CUSTOM_BUILD_TYPE
    CUSTOM_BUILD_TYPE := UNOFFICIAL
endif

# Set all versions
CUSTOM_VERSION := KAF-$(CUSTOM_BUILD)$(CUSTOM_POSTFIX)-$(shell sed '5q;d' .repo/manifest.xml | sed 's/.*tags\/\(.*\)".*/\1/')-$(CUSTOM_BUILD_TYPE)
CUSTOM_MOD_VERSION := $(CUSTOM_VERSION)

PRODUCT_PROPERTY_OVERRIDES += \
    BUILD_DISPLAY_ID=$(BUILD_ID) \
    ro.custom.version=$(CUSTOM_VERSION) \
    ro.modversion=$(CUSTOM_VERSION) \
    ro.custom.buildtype=$(CUSTOM_BUILD_TYPE)

EXTENDED_POST_PROCESS_PROPS := vendor/custom/tools/custom_process_props.py

-include vendor/custom/config/qualcomm.mk